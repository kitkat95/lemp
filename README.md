# LEMP Starter Kit

This repository contains a little `docker-compose` configuration to start a `LEMP (Linux, Nginx, MariaDB, PHP)` stack.

## Details

The following versions are used.

* PHP 7.4 (FPM)
* Nginx latest
* MariaDB 10.2.32

## Configuration

The Nginx configuration can be found in `config/nginx/`.

You can also set the following environment variables, for example in the included `.env` file:

| Key | Description |
|-----|-------------|
|APP_NAME|The name used when creating a container.|
|MYSQL_DATABASE|The name of the database created in mariadb.|
|MYSQL_USER|The username for database access.|
|MYSQL_PASSWORD|The user password for database access.|
|MYSQL_ROOT_PASSWORD|The password for root user.|

## Usage

- `cp .env.example .env`
- `docker-compose up`
- `docker-compose exec web bash`

If you want to remove db persisting, remove `volumes` in mariadb container.